from django import forms
#from . models import Post
import datetime

class Homeform(forms.Form):
    post_id = forms.IntegerField()
    post = forms.CharField()
    posted_by = forms.IntegerField()
    posted_on = forms.IntegerField()
    privacy = forms.CharField()


class PostForm(forms.Form):
    post = forms.CharField(label='Post', widget=forms.Textarea)


class CommentForm(forms.Form):
    comment = forms.CharField(label='comment')


react_choices = [['None','None'],['like','like'],['dislike','dislike']]
class ReactForm(forms.Form):
    react = forms.ChoiceField(choices=react_choices)


class CreateEventForm(forms.Form):
    event_name = forms.CharField(label='event_name')
    event_place = forms.CharField(label='event_place')
    event_date_time = forms.CharField(label='event_date_time', widget=forms.SelectDateWidget
                                      , initial=datetime.date.today()) ### Needs a further checking
    description = forms.CharField(label='description', widget=forms.Textarea, required=False)


class AdminCheck(forms.Form):
    admin_check = forms.ChoiceField(choices=[['n','No'], ['y','Yes']])


class EventForm(forms.Form):
    state = forms.ChoiceField(
        choices=[['interested', 'Interested'], ['going', 'Going'], ['not going', 'Not Going']])

