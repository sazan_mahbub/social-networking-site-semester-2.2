from django.conf.urls import url, include
from django.contrib.auth.views import login
from django.contrib import admin
from events import views

urlpatterns = [
    #url(r'^$', HomeView.as_view()),
    url(r'^all_events/$', views.view_all_events, name='all_event_list'),
    url(r'^event_invitations/$', views.event_invitations, name='event_invitations'),
    url(r'^create_event/$', views.CreateEventView.as_view(), name='create_event'),
    url(r'^event/(?P<event_id>\d+)/post_list/$', views.view_event_post_list, name='event_post_list'),
    url(r'^event/(?P<event_id>\d+)/members/$', views.event_members, name='event_members'),
    url(r'^event/(?P<event_id>\d+)/invite_member/$', views.invite_member, name='invite_member'),
    url(r'^event/(?P<event_id>\d+)/event_participation/$', views.event_state_change, name='event_state_change'),
    url(r'^event/(?P<event_id>\d+)/member_invited/(?P<user_id>\d+)$', views.member_invited, name='member_invited'),
    url(r'^event/(?P<event_id>\d+)/post/$', views.EventPostView.as_view(), name='event_post'),
    url(r'^event/(?P<event_id>\d+)/post/(?P<post_id>\d+)/$', views.EventPostDetailsView.as_view(), name='event_post_details'),
    #url(r'^post_comment/$', views.post_comment, name='comment'),
    #url(r'^post_react/$', views.post_react, name='react'),
]
