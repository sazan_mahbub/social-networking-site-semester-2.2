from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import TemplateView
from events.forms import Homeform, PostForm
#from . import models
from django.db import connection, transaction
from collections import namedtuple
import datetime
from events import forms


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


class EventPostView(TemplateView):
    template_name = 'events/post.html'
    template_name2 = 'events/event_post_successful.html'

    def get(self, request, event_id):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        form = forms.PostForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request, event_id):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        form = PostForm(request.POST)
        if form.is_valid():
            post = form.cleaned_data['post']
            posted_by = request.session['user_id']
            media_id = ''
            # insertion in database
            with connection.cursor() as cursor:
                cursor.execute('''INSERT INTO event_post(event_id,posted_by_id,event_post_text,media_id) 
                                VALUES (%s, %s, %s, %s)''', [event_id, posted_by, post, media_id])
                cursor.execute('''select max(event_post_id) post_id from event_post''')
                post = namedtuplefetchall(cursor)[0]
                args = {
                    'event_id': int(event_id),
                    'post': post
                }
        else:
            form = PostForm()
            return render(request, self.template_name, {'form': form})

        return render(request, self.template_name2, args)


class EventPostDetailsView(TemplateView):
    template_name = 'events/event_post_details.html'
    post_id = None
    post_details = None
    comment_form = None
    react_form = None
    args = None

    def get(self, request, event_id, post_id):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        with connection.cursor() as cursor:
            cursor.execute('''select event_id from event_post where event_post_id = %s''', [post_id])
            temp = namedtuplefetchall(cursor)[0].EVENT_ID
            #print('{}-{}'.format(type(temp), type(group_id)))
            if temp!=int(event_id):
                return HttpResponse('<h1>Error: No such event post.</h1>')

            cursor.execute('''select u1.user_id u1_id, u1.username u1name, p.*
                              from event_post p join users u1 on(p.posted_by_id=u1.user_id)
                              where event_id=%s and event_post_id = %s''', [event_id, post_id])
            self.post_id = post_id
            self.post_details = namedtuplefetchall(cursor)[0]
            #print('{}-{}'.format(post_id,self.post_details))
            self.comment_form = forms.CommentForm()
            self.react_form = forms.ReactForm()
            cursor.execute('''select count(*) cnt from event_post_reaction where event_post_id=%s and react='like' ''',
                           [post_id])
            like_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select count(*) cnt from event_post_reaction where event_post_id=%s and react='dislike' ''',
                           [post_id])
            dislike_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select count(*) cnt from event_post_comment where post_id=%s
                              and comment_text is not NULL ''',
                           [post_id])
            comment_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select u.username, r.comment_text from event_post_comment r 
                            join users u on(r.user_id=u.user_id) 
                            where r.post_id=%s order by r.event_post_comment_id desc''',
                           [post_id])
            all_comments = namedtuplefetchall(cursor)
            cursor.execute('''UPDATE event_post SET like_count=%s,dislike_count=%s,comment_count=%s 
                            where event_post_id=%s''',[like_count,dislike_count,comment_count,post_id])

            #print('{}-{}'.format(post_id,all_comments))
            self.args = {
                'my_id': request.session['user_id'],
                'post_id': self.post_id,
                'post': self.post_details,
                'comment_form': self.comment_form,
                'react_form': self.react_form,
                'like_count': like_count,
                'dislike_count': dislike_count,
                'comment_count': comment_count,
                'all_comments': all_comments,
            }
        return render(request, self.template_name, self.args)

    def post(self, request, event_id, post_id):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        #print(self.args)
        comment_form = forms.CommentForm(request.POST)
        react_form = forms.ReactForm(request.POST)
        if comment_form.is_valid() or react_form.is_valid():
            comment = ''
            react  = ''
            media = ''
            if comment_form.is_valid():
                comment = comment_form.cleaned_data['comment']
            if react_form.is_valid():
                react = react_form.cleaned_data['react']
            commented_by = request.session['user_id']
            print('{}-{}-{}'.format(comment, react, commented_by))
            # handling database
            with connection.cursor() as cursor:
                try:
                    if react != '':
                        cursor.execute('''insert into event_post_reaction(user_id, event_post_id, react) 
                                      values(%s,%s,%s)''', [request.session['user_id'], post_id, react])
                    elif comment != '':
                        cursor.execute('''insert into event_post_comment(user_id, post_id, comment_text, media_id) 
                                      values(%s,%s,%s,%s)''', [request.session['user_id'], post_id, comment, media])
                except:
                    if react!='':
                        if react == "None": react = ''
                        cursor.execute('''update event_post_reaction set react=%s 
                                          where user_id=%s and event_post_id=%s''',
                                       [react, request.session['user_id'], post_id])
                    print('already in hoss!')
                cursor.execute('''select u1.user_id u1_id, u1.username u1name, p.*
                                  from event_post p join users u1 on(p.posted_by_id=u1.user_id)
                                  where event_post_id = %s''', [post_id])
                self.post_id = post_id
                self.post_details = namedtuplefetchall(cursor)[0]
                self.comment_form = forms.CommentForm()
                self.react_form = forms.ReactForm()
                cursor.execute(
                    '''select count(*) cnt from event_post_reaction where event_post_id=%s and react='like' ''',
                    [post_id])
                like_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute(
                    '''select count(*) cnt from event_post_reaction where event_post_id=%s and react='dislike' ''',
                    [post_id])
                dislike_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''select count(*) cnt from event_post_comment where post_id=%s
                                              and comment_text is not NULL ''',
                               [post_id])
                comment_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''select u.username, r.comment_text from event_post_comment r 
                                join users u on(r.user_id=u.user_id) 
                                where r.post_id=%s order by r.event_post_comment_id desc''',
                               [post_id])
                all_comments = namedtuplefetchall(cursor)
                self.args = {
                    'my_id': request.session['user_id'],
                    'post_id': self.post_id,
                    'post': self.post_details,
                    'comment_form': self.comment_form,
                    'react_form': self.react_form,
                    'like_count': like_count,
                    'dislike_count': dislike_count,
                    'comment_count': comment_count,
                    'all_comments': all_comments,
                }
            return render(request, self.template_name, self.args)
        else:
            pass


def view_event_post_list(request, event_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'events/event_page.html'
    with connection.cursor() as cursor:
        cursor.execute('''SELECT * FROM event_post e join users u on(e.posted_by_id=u.user_id)
                        where event_id=%s ORDER BY event_post_id DESC''',[event_id])
        posts = namedtuplefetchall(cursor)

        cursor.execute('''SELECT count(*) cnt FROM event_participants 
                       where event_id=%s and state_of_participation='going' ''',[event_id])
        going_number = namedtuplefetchall(cursor)[0].CNT
        cursor.execute('''SELECT count(*) cnt FROM event_participants 
                       where event_id=%s and state_of_participation='not going' ''',[event_id])
        not_going_number = namedtuplefetchall(cursor)[0].CNT
        cursor.execute('''SELECT count(*) cnt FROM event_participants 
                       where event_id=%s and state_of_participation='interested' ''',[event_id])
        interested_number = namedtuplefetchall(cursor)[0].CNT
        cursor.execute('''SELECT count(*) cnt FROM event_participants 
                        where event_id=%s and state_of_participation='invited' ''', [event_id])
        invited_number = namedtuplefetchall(cursor)[0].CNT

        cursor.execute('''SELECT * FROM event_table e join users u on(u.user_id=e.creator_id) 
                        where event_id=%s''',[event_id])
        event_details = namedtuplefetchall(cursor)[0]
        cursor.execute('''update event_table SET going_number=%s, not_going_number=%s, interested_number=%s, 
                        invited_number=%s where event_id=%s''',
                       [going_number,not_going_number,interested_number,invited_number,event_id])
        form = forms.EventForm()
        args = {
            'form': form,
            'event_id': event_id,
            'event_details': event_details,
            'posts': posts,
            'going_number': going_number,
            'not_going_number': not_going_number,
            'interested_number': interested_number,
            'invited_number': invited_number,
        }
    return render(request, template_name, args)


def view_all_events(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'events/event_list.html'
    with connection.cursor() as cursor:
        cursor.execute('''SELECT * FROM event_table ORDER BY event_id desc''')
        event_list = namedtuplefetchall(cursor)
        #print(event_list)
        args = {
            'event_list': event_list
        }
    return render(request, template_name, args)


class CreateEventView(TemplateView):
    template_name = 'events/create_event.html'
    template_name2 = 'events/event_create_successful.html'

    def get(self, request):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        form = forms.CreateEventForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        form = forms.CreateEventForm(request.POST)
        if form.is_valid():
            event_name = form.cleaned_data['event_name']
            event_place = form.cleaned_data['event_place']
            description = form.cleaned_data['description']
            event_date_time = str(form.cleaned_data['event_date_time'])
            print(event_date_time)
            # insertion in database
            with connection.cursor() as cursor:
                """cursor.execute('''insert into event_table(creator_id,event_date_time,event_place,description,event_name) 
                                  values(%s,%s,%s,%s,%s);''',
                                 [request.session['user_id'],event_date_time,event_place,description,event_name])
                cursor.execute('''select max(event_id) event_id from event_table''')
                event_id = namedtuplefetchall(cursor)[0].EVENT_ID"""
                ###
                event_id = cursor.callfunc('create_event',int,
                                    [request.session['user_id'],event_date_time,event_place,description,event_name])

                print('event_id: {}'.format(event_id))
                args = {
                    'event_id':event_id
                }
        else:
            form = forms.CreateEventForm()
            return render(request, self.template_name, {'form': form})

        return render(request, self.template_name2, args)


def event_members(request, event_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'events/event_member_list.html'
    with connection.cursor() as cursor:
        cursor.execute('''select user_id, username, STATE_OF_PARTICIPATION from event_participants g join users u on(g.member_id=u.user_id) 
                        where event_id=%s and u.account_state='active' ORDER BY u.username''',[event_id])
        members = namedtuplefetchall(cursor)
        print(members)
        args = {
            'members': members
        }
    return render(request, template_name, args)


def invite_member(request, event_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'events/invite_member.html'
    with connection.cursor() as cursor:
        cursor.execute('''select * from users where account_state='active' and user_id not in 
                        (select member_id from event_participants where event_id=%s) ORDER BY username''',[event_id])
        users = namedtuplefetchall(cursor)
        args = {
            'my_id': request.session['user_id'],
            'event_id': event_id,
            'users': users,
        }
    return render(request, template_name, args)


def member_invited(request, event_id, user_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'events/member_invited.html'
    state_of_participation = 'invited'
    with connection.cursor() as cursor:
        cursor.execute('''insert into event_participants(event_id,member_id,state_of_participation) values(%s,%s,%s)''',
                       [event_id,user_id,state_of_participation])
        args = {
            'event_id': event_id,
        }
    return render(request, template_name, args)


def event_state_change(request, event_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'events/event_state_change.html'
    my_id = request.session['user_id']
    if request.method == 'POST':
        form = forms.EventForm(request.POST)
        if form.is_valid():
            state = form.cleaned_data['state']
            with connection.cursor() as cursor:
                try:
                    cursor.execute(
                        ''' insert into event_participants(event_id,member_id,state_of_participation)
                        values(%s,%s,%s)''',
                        [event_id, my_id, state])
                except:
                    cursor.execute(
                        ''' update event_participants set state_of_participation=%s where event_id=%s and member_id=%s''',
                        [state,event_id,my_id])
                args = {
                    'event_id': event_id,
                    'state': state,
                }
            return render(request, template_name, args)
        else:
            HttpResponse('Error')


def event_invitations(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'events/event_invitations.html'
    my_id=request.session['user_id']
    with connection.cursor() as cursor:
        cursor.execute(
            ''' select * from event_participants where member_id=%s and state_of_participation='invited' ''',
            [my_id])
        events = namedtuplefetchall(cursor)
        args={
            'events': events,
        }
        return render(request, template_name, args)