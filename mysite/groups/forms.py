from django import forms
#from . models import Post

'''
class HomeModelForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = [
                'post_text',
                'posted_by_id',
                'posted_on_id',
                ''
                ]
'''

class Homeform(forms.Form):
    post_id = forms.IntegerField()
    post = forms.CharField()
    posted_by = forms.IntegerField()
    posted_on = forms.IntegerField()
    privacy = forms.CharField()


class PostForm(forms.Form):
    post = forms.CharField(label='Post', widget=forms.Textarea)
    #posted_by = forms.IntegerField() # this needs to be gone
    #posted_on = forms.IntegerField()


class CommentForm(forms.Form):
    comment = forms.CharField(label='comment')


react_choices = [['None','None'],['like','like'],['dislike','dislike']]
class ReactForm(forms.Form):
    react = forms.ChoiceField(choices=react_choices)


privacy_choices = [['public','public'],['closed','closed'],['secret','secret']]
class CreateGroupForm(forms.Form):
    group_name = forms.CharField(label='group_name')
    group_privacy = forms.ChoiceField(label='group_privacy', choices=privacy_choices)
    description = forms.CharField(label='description', widget=forms.Textarea, required=False)


class AdminCheck(forms.Form):
    admin_check = forms.ChoiceField(choices=[['n','No'], ['y','Yes']])