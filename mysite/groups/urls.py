from django.conf.urls import url, include
from django.contrib.auth.views import login
from django.contrib import admin
from groups import views

urlpatterns = [
    #url(r'^$', HomeView.as_view()),
    url(r'^all_groups/$', views.view_all_groups, name='all_group_list'),
    url(r'^create_group/$', views.CreateGroupView.as_view(), name='create_group'),
    url(r'^group/(?P<group_id>\d+)/details/$', views.view_group_post_list, name='group_post_list'),
    url(r'^group/(?P<group_id>\d+)/members/$', views.group_members, name='group_members'),
    url(r'^group/(?P<group_id>\d+)/add_member/$', views.add_member, name='add_member'),
    url(r'^group/(?P<group_id>\d+)/member/(?P<user_id>\d+)$', views.member_added, name='member_added'),
    url(r'^group/(?P<group_id>\d+)/post/$', views.GroupPostView.as_view(), name='group_post'),
    url(r'^group/(?P<group_id>\d+)/post/(?P<post_id>\d+)/$', views.GroupPostDetailsView.as_view(), name='group_post_details'),
    #url(r'^post_comment/$', views.post_comment, name='comment'),
    #url(r'^post_react/$', views.post_react, name='react'),
]
