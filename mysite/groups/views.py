from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import TemplateView
from groups.forms import Homeform, PostForm
#from . import models
from django.db import connection, transaction
from collections import namedtuple
import datetime
from groups import forms


# Create your views here.

class HomeView(TemplateView):
    template_name = 'groups/post.html'
    template_name2 = 'groups/posts_page.html'

    def get(self, request):
        form = Homeform()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = Homeform(request.POST)
        if form.is_valid():
            post_id = form.cleaned_data['post_id']
            post = form.cleaned_data['post']
            posted_by = form.cleaned_data['posted_by']
            posted_on = form.cleaned_data['posted_on']
            privacy = form.cleaned_data['privacy']

            # post_time = datetime.datetime.now()

            form = Homeform()
            # return redirect('home:home')
            # users = self.my_custom_sql()
            # posted_by =self.get_user(posted_by)[0]
            # posted_on =self.get_user(posted_on)[0]

            # insertion in database
            with connection.cursor() as cursor:
                cursor.execute('''INSERT INTO post(posted_by_id, posted_on_id, post_text, privacy) VALUES (
                %s, %s, %s, %s)''', [posted_by, posted_on, post, privacy])
                cursor.execute('''SELECT * FROM POST''')
                posts = self.namedtuplefetchall(cursor)

        # args = {'post_id':post_id, 'post':post, 'posted_by':posted_by, 'posted_on':posted_on, 'privacy':privacy, 'post_time':post_time}
        args = {'posts': posts}
        return render(request, self.template_name2, args)

    def my_custom_sql(self):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT * FROM users''')
            row = self.namedtuplefetchall(cursor)

        for i in row:
            print(i.USERNAME)
        return row

    def get_user(self, u_id):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT * FROM users where user_id = %s''', [u_id])
            row = self.namedtuplefetchall(cursor)

        for i in row:
            print(i.USERNAME)
        return row

    def namedtuplefetchall(self, cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


class GroupPostView(TemplateView):
    template_name = 'groups/post.html'
    template_name2 = 'groups/group_post_successful.html'

    def get(self, request, group_id):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        form = forms.PostForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request, group_id):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        form = PostForm(request.POST)
        if form.is_valid():
            post = form.cleaned_data['post']
            posted_by = request.session['user_id']
            #posted_on = group_id
            #privacy = form.cleaned_data['privacy']
            # insertion in database
            with connection.cursor() as cursor:
                cursor.execute('''INSERT INTO group_post(group_id, posted_by_id, group_post_text) VALUES (
                %s, %s, %s)''', [group_id, posted_by, post])
                cursor.execute('''select max(group_post_id) post_id from group_post''')
                post = namedtuplefetchall(cursor)[0]
                args = {
                    'group_id': int(group_id),
                    'post': post
                }
        else:
            form = PostForm()
            return render(request, self.template_name, {'form': form})

        return render(request, self.template_name2, args)


class GroupPostDetailsView(TemplateView):
    template_name = 'groups/group_post_details.html'
    post_id = None
    post_details = None
    comment_form = None
    react_form = None
    args = None

    def get(self, request, group_id, post_id):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        with connection.cursor() as cursor:
            cursor.execute('''select group_id from group_post where group_post_id = %s''', [post_id])
            temp = namedtuplefetchall(cursor)[0].GROUP_ID
            #print('{}-{}'.format(type(temp), type(group_id)))
            if temp!=int(group_id):
                return HttpResponse('<h1>Error: No such group post.</h1>')

            cursor.execute('''select u1.user_id u1_id, u1.username u1name, p.*
                              from group_post p join users u1 on(p.posted_by_id=u1.user_id)
                              where group_id=%s and group_post_id = %s''', [group_id, post_id])
            self.post_id = post_id
            self.post_details = namedtuplefetchall(cursor)[0]
            print(self.post_details)
            self.comment_form = forms.CommentForm()
            self.react_form = forms.ReactForm()
            cursor.execute('''select count(*) cnt from group_post_reaction where group_post_id=%s and react='like' ''',
                           [post_id])
            like_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select count(*) cnt from group_post_reaction where group_post_id=%s and react='dislike' ''',
                           [post_id])
            dislike_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select count(*) cnt from group_post_comment where post_id=%s
                              and comment_text is not NULL ''',
                           [post_id])
            comment_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select u.username, r.comment_text from group_post_comment r 
                            join users u on(r.user_id=u.user_id) 
                            where r.post_id=%s order by r.group_post_comment_id desc''',
                           [post_id])
            all_comments = namedtuplefetchall(cursor)
            self.args = {
                'my_id': request.session['user_id'],
                'post_id': self.post_id,
                'post': self.post_details,
                'comment_form': self.comment_form,
                'react_form': self.react_form,
                'like_count': like_count,
                'dislike_count': dislike_count,
                'comment_count': comment_count,
                'all_comments': all_comments,
            }
        return render(request, self.template_name, self.args)

    def post(self, request, group_id, post_id):
        #print(self.args)
        comment_form = forms.CommentForm(request.POST)
        react_form = forms.ReactForm(request.POST)
        if comment_form.is_valid() or react_form.is_valid():
            comment = ''
            react  = ''
            if comment_form.is_valid():
                comment = comment_form.cleaned_data['comment']
            if react_form.is_valid():
                react = react_form.cleaned_data['react']
            commented_by = request.session['user_id']
            print('{}-{}-{}'.format(comment, react, commented_by))
            # handling database
            with connection.cursor() as cursor:
                try:
                    if react != '':
                        cursor.execute('''insert into group_post_reaction(user_id, group_post_id, react) 
                                      values(%s,%s,%s)''', [request.session['user_id'], post_id, react])
                    elif comment != '':
                        cursor.execute('''insert into group_post_comment(user_id, post_id, comment_text) 
                                      values(%s,%s,%s)''', [request.session['user_id'], post_id, comment])
                except:
                    if react!='':
                        if react == "None": react = ''
                        cursor.execute('''update group_post_reaction set react=%s 
                                          where user_id=%s and group_post_id=%s''',
                                       [react, request.session['user_id'], post_id])
                    print('already in hoss!')
                cursor.execute('''select u1.user_id u1_id, u1.username u1name, p.*
                                  from group_post p join users u1 on(p.posted_by_id=u1.user_id)
                                  where group_post_id = %s''', [post_id])
                self.post_id = post_id
                self.post_details = namedtuplefetchall(cursor)[0]
                self.comment_form = forms.CommentForm()
                self.react_form = forms.ReactForm()
                cursor.execute(
                    '''select count(*) cnt from group_post_reaction where group_post_id=%s and react='like' ''',
                    [post_id])
                like_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute(
                    '''select count(*) cnt from group_post_reaction where group_post_id=%s and react='dislike' ''',
                    [post_id])
                dislike_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''select count(*) cnt from group_post_comment where post_id=%s
                                              and comment_text is not NULL ''',
                               [post_id])
                comment_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''select u.username, r.comment_text from group_post_comment r 
                                join users u on(r.user_id=u.user_id) 
                                where r.post_id=%s order by r.group_post_comment_id desc''',
                               [post_id])
                all_comments = namedtuplefetchall(cursor)
                self.args = {
                    'post_id': self.post_id,
                    'post': self.post_details,
                    'comment_form': self.comment_form,
                    'react_form': self.react_form,
                    'like_count': like_count,
                    'dislike_count': dislike_count,
                    'comment_count': comment_count,
                    'all_comments': all_comments,
                }
            return render(request, self.template_name, self.args)
        else:
            pass


def view_group_post_list(request, group_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'groups/posts_page.html'
    with connection.cursor() as cursor:
        cursor.execute('''SELECT * FROM group_post where group_id=%s ORDER BY group_post_id DESC''',[group_id])
        posts = namedtuplefetchall(cursor)
        cursor.execute('''SELECT count(*) cnt FROM group_member where group_id=%s''',[group_id])
        member_number = namedtuplefetchall(cursor)[0].CNT
        cursor.execute('''SELECT * FROM groups where group_id=%s''',[group_id])
        group_details = namedtuplefetchall(cursor)[0]
        cursor.execute('''update groups SET member_number=%s where group_id=%s''',[member_number,group_id])
        cursor.execute('''SELECT user_id,username FROM group_member m join users u on(u.user_id=m.member_id) 
                        where m.group_id=%s and m.admin='y' ''',[group_id])
        admins = namedtuplefetchall(cursor)
        #print(posts)
        args = {
            'group_id': group_id,
            'group_details': group_details,
            'posts': posts,
            'member_number':member_number,
            'admins':admins
        }
    return render(request, template_name, args)


def view_all_groups(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'groups/group_list.html'
    with connection.cursor() as cursor:
        cursor.execute('''SELECT * FROM groups ORDER BY group_id desc''')
        group_list = namedtuplefetchall(cursor)
        #print(group_list)
        args = {
            'group_list': group_list
        }
    return render(request, template_name, args)


class CreateGroupView(TemplateView):
    template_name = 'groups/create_group.html'
    template_name2 = 'groups/group_create_successful.html'

    def get(self, request):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        form = forms.CreateGroupForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        form = forms.CreateGroupForm(request.POST)
        if form.is_valid():
            group_name = form.cleaned_data['group_name']
            group_privacy = form.cleaned_data['group_privacy']
            description = form.cleaned_data['description']
            # insertion in database
            with connection.cursor() as cursor:
                """???
                cursor.execute('''insert into groups(creator_id,description,group_name,group_privacy) 
                                 values(%s,%s,%s,%s);''',
                                [request.session['user_id'],description,group_name,group_privacy])
                cursor.execute('''select max(group_id) group_id from groups''')
                group_id = namedtuplefetchall(cursor)[0].GROUP_ID
                ???"""
                group_id = cursor.callfunc('create_group', int,
                                           [request.session['user_id'],description,group_name,group_privacy])
                print('group_id: {}'.format(group_id))
                args = {
                    'group_id':group_id
                }
        else:
            form = forms.CreateGroupForm()
            return render(request, self.template_name, {'form': form})

        return render(request, self.template_name2, args)


def group_members(request, group_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'groups/group_member_list.html'
    with connection.cursor() as cursor:
        cursor.execute('''select user_id, username from group_member g join users u on(g.member_id=u.user_id) 
                        where group_id=%s and u.account_state='active' ORDER BY u.username''',[group_id])
        members = namedtuplefetchall(cursor)

        #print(members)
        args = {
            'members': members
        }
    return render(request, template_name, args)


def add_member(request, group_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'groups/add_member.html'
    with connection.cursor() as cursor:
        cursor.execute('''select * from users where account_state='active' and user_id not in 
                        (select member_id from group_member where group_id=%s) ORDER BY first_name''',[group_id])
        users = namedtuplefetchall(cursor)
        #admin_form = forms.AdminCheck()
        args = {
            'my_id': request.session['user_id'],
            'group_id': group_id,
            'users': users,
            #'admin_form': admin_form
        }
    return render(request, template_name, args)


def member_added(request, group_id, user_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'groups/member_added.html'
    admin = 'n'
    with connection.cursor() as cursor:
        cursor.execute('''insert into group_member(group_id,member_id,admin) values(%s,%s,%s)''',
                       [group_id,user_id,admin])
        args = {
            'group_id': group_id,
        }
    return render(request, template_name, args)
