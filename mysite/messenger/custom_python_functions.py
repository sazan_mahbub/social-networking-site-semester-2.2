from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import TemplateView
#from . import models
from django.db import connection, transaction
from collections import namedtuple
import datetime
from messenger import forms


def namedtuplefetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

#react_choices = [['None','None'],['like','like'],['dislike','dislike']]
def get_users():
    with connection.cursor() as cursor:
        cursor.execute('''select * from users order by username''')
        temp = namedtuplefetchall(cursor)
        users = []
        for t in temp:
            users.append([t.USER_ID,t.USERNAME])
