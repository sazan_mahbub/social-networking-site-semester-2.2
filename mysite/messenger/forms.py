from django import forms
#from . models import Post
import datetime
from messenger.custom_python_functions import get_users

"""
class Homeform(forms.Form):
    post_id = forms.IntegerField()
    post = forms.CharField()
    posted_by = forms.IntegerField()
    posted_on = forms.IntegerField()
    privacy = forms.CharField()


class PostForm(forms.Form):
    post = forms.CharField(label='Post', widget=forms.Textarea)


class CommentForm(forms.Form):
    comment = forms.CharField(label='comment')


react_choices = [['None','None'],['like','like'],['dislike','dislike']]
class ReactForm(forms.Form):
    react = forms.ChoiceField(choices=react_choices)


class CreateEventForm(forms.Form):
    event_name = forms.CharField(label='event_name')
    event_place = forms.CharField(label='event_place')
    event_date_time = forms.CharField(label='event_date_time', widget=forms.SelectDateWidget
                                      , initial=datetime.date.today()) ### Needs a further checking
    description = forms.CharField(label='description', widget=forms.Textarea, required=False)


class AdminCheck(forms.Form):
    admin_check = forms.ChoiceField(choices=[['n','No'], ['y','Yes']])

"""


class MessageFormMedia(forms.Form):
    media = forms.CharField(label='media')


class MessageFormText(forms.Form):
    text = forms.CharField(label='text')


class MessageForm(forms.Form):
    #media = forms.CharField(label='media', required=False)
    text = forms.CharField(label='text')


#react_choices = [['None','None'],['like','like'],['dislike','dislike']]
class GroupConvoForm(forms.Form):
    #members = None

    def __init__(self, *args, **kwargs):
        temp = kwargs.pop('users')
        super(GroupConvoForm, self).__init__(*args, **kwargs)
        self.fields['members'] = forms.MultipleChoiceField(label="members",
                                                           choices=[[t.USER_ID, t.USERNAME] for t in temp],
                                                           widget=forms.CheckboxSelectMultiple)