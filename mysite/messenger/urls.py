from django.conf.urls import url, include
from django.contrib.auth.views import login
from django.contrib import admin
from messenger import views

urlpatterns = [
    #url(r'^$', HomeView.as_view()),
    url(r'^$', views.messenger_home, name='messenger'),
    url(r'^inbox/$', views.inbox, name='inbox'),
    url(r'^other_users/$', views.other_users, name='other_users'),
    url(r'^message_user/(?P<uid>\d+)/$', views.message_user, name='message_user'),
    url(r'^start_group_convo/$', views.StartGroupConvoView.as_view(), name='start_group_convo'),
    url(r'^conversation/(?P<conv_id>\d+)/$', views.ConversationView.as_view(), name='conversation'),
    #url(r'^other_people_list/$', views.other_user_list, name='other_users'),

    ####
    ##url(r'^event/(?P<event_id>\d+)/post_list/$', views.view_event_post_list, name='event_post_list'),
    #url(r'^event/(?P<event_id>\d+)/members/$', views.event_members, name='event_members'),
    #url(r'^event/(?P<event_id>\d+)/invite_member/$', views.invite_member, name='invite_member'),
    #url(r'^event/(?P<event_id>\d+)/member_invited/(?P<user_id>\d+)$', views.member_invited, name='member_invited'),
    #url(r'^event/(?P<event_id>\d+)/post/$', views.EventPostView.as_view(), name='event_post'),
    #url(r'^event/(?P<event_id>\d+)/post/(?P<post_id>\d+)/$', views.EventPostDetailsView.as_view(), name='event_post_details'),
    #url(r'^post_comment/$', views.post_comment, name='comment'),
    #url(r'^post_react/$', views.post_react, name='react'),
]
