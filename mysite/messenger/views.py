from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import TemplateView
#from . import models
from django.db import connection, transaction
from collections import namedtuple
import datetime
from messenger import forms
import cx_Oracle


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


"""
class EventPostView(TemplateView):
    template_name = 'events/post.html'
    template_name2 = 'events/event_post_successful.html'

    def get(self, request, event_id):
        form = forms.PostForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request, event_id):
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.cleaned_data['post']
            posted_by = request.session['user_id']
            media_id = ''
            # insertion in database
            with connection.cursor() as cursor:
                cursor.execute('''INSERT INTO event_post(event_id,posted_by_id,event_post_text,media_id) 
                                VALUES (%s, %s, %s, %s)''', [event_id, posted_by, post, media_id])
                cursor.execute('''select max(event_post_id) post_id from event_post''')
                post = namedtuplefetchall(cursor)[0]
                args = {
                    'event_id': int(event_id),
                    'post': post
                }
        else:
            form = PostForm()
            return render(request, self.template_name, {'form': form})

        return render(request, self.template_name2, args)


class EventPostDetailsView(TemplateView):
    template_name = 'events/event_post_details.html'
    post_id = None
    post_details = None
    comment_form = None
    react_form = None
    args = None

    def get(self, request, event_id, post_id):
        with connection.cursor() as cursor:
            cursor.execute('''select event_id from event_post where event_post_id = %s''', [post_id])
            temp = namedtuplefetchall(cursor)[0].EVENT_ID
            #print('{}-{}'.format(type(temp), type(group_id)))
            if temp!=int(event_id):
                return HttpResponse('<h1>Error: No such event post.</h1>')

            cursor.execute('''select u1.user_id u1_id, u1.username u1name, p.*
                              from event_post p join users u1 on(p.posted_by_id=u1.user_id)
                              where event_id=%s and event_post_id = %s''', [event_id, post_id])
            self.post_id = post_id
            self.post_details = namedtuplefetchall(cursor)[0]
            #print('{}-{}'.format(post_id,self.post_details))
            self.comment_form = forms.CommentForm()
            self.react_form = forms.ReactForm()
            cursor.execute('''select count(*) cnt from event_post_reaction where event_post_id=%s and react='like' ''',
                           [post_id])
            like_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select count(*) cnt from event_post_reaction where event_post_id=%s and react='dislike' ''',
                           [post_id])
            dislike_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select count(*) cnt from event_post_comment where post_id=%s
                              and comment_text is not NULL ''',
                           [post_id])
            comment_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select u.username, r.comment_text from event_post_comment r 
                            join users u on(r.user_id=u.user_id) 
                            where r.post_id=%s order by r.event_post_comment_id desc''',
                           [post_id])
            all_comments = namedtuplefetchall(cursor)
            cursor.execute('''UPDATE event_post SET like_count=%s,dislike_count=%s,comment_count=%s 
                            where event_post_id=%s''',[like_count,dislike_count,comment_count,post_id])

            #print('{}-{}'.format(post_id,all_comments))
            self.args = {
                'post_id': self.post_id,
                'post': self.post_details,
                'comment_form': self.comment_form,
                'react_form': self.react_form,
                'like_count': like_count,
                'dislike_count': dislike_count,
                'comment_count': comment_count,
                'all_comments': all_comments,
            }
        return render(request, self.template_name, self.args)

    def post(self, request, event_id, post_id):
        #print(self.args)
        comment_form = forms.CommentForm(request.POST)
        react_form = forms.ReactForm(request.POST)
        if comment_form.is_valid() or react_form.is_valid():
            comment = ''
            react  = ''
            media = ''
            if comment_form.is_valid():
                comment = comment_form.cleaned_data['comment']
            if react_form.is_valid():
                react = react_form.cleaned_data['react']
            commented_by = request.session['user_id']
            print('{}-{}-{}'.format(comment, react, commented_by))
            # handling database
            with connection.cursor() as cursor:
                try:
                    if react != '':
                        cursor.execute('''insert into event_post_reaction(user_id, event_post_id, react) 
                                      values(%s,%s,%s)''', [request.session['user_id'], post_id, react])
                    elif comment != '':
                        cursor.execute('''insert into event_post_comment(user_id, post_id, comment_text, media_id) 
                                      values(%s,%s,%s,%s)''', [request.session['user_id'], post_id, comment, media])
                except:
                    if react!='':
                        if react == "None": react = ''
                        cursor.execute('''update event_post_reaction set react=%s 
                                          where user_id=%s and event_post_id=%s''',
                                       [react, request.session['user_id'], post_id])
                    print('already in hoss!')
                cursor.execute('''select u1.user_id u1_id, u1.username u1name, p.*
                                  from event_post p join users u1 on(p.posted_by_id=u1.user_id)
                                  where event_post_id = %s''', [post_id])
                self.post_id = post_id
                self.post_details = namedtuplefetchall(cursor)[0]
                self.comment_form = forms.CommentForm()
                self.react_form = forms.ReactForm()
                cursor.execute(
                    '''select count(*) cnt from event_post_reaction where event_post_id=%s and react='like' ''',
                    [post_id])
                like_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute(
                    '''select count(*) cnt from event_post_reaction where event_post_id=%s and react='dislike' ''',
                    [post_id])
                dislike_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''select count(*) cnt from event_post_comment where post_id=%s
                                              and comment_text is not NULL ''',
                               [post_id])
                comment_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''select u.username, r.comment_text from event_post_comment r 
                                join users u on(r.user_id=u.user_id) 
                                where r.post_id=%s order by r.event_post_comment_id desc''',
                               [post_id])
                all_comments = namedtuplefetchall(cursor)
                self.args = {
                    'post_id': self.post_id,
                    'post': self.post_details,
                    'comment_form': self.comment_form,
                    'react_form': self.react_form,
                    'like_count': like_count,
                    'dislike_count': dislike_count,
                    'comment_count': comment_count,
                    'all_comments': all_comments,
                }
            return render(request, self.template_name, self.args)
        else:
            pass


def view_event_post_list(request, event_id):
    template_name = 'events/event_page.html'
    with connection.cursor() as cursor:
        cursor.execute('''SELECT * FROM event_post where event_id=%s ORDER BY event_post_id DESC''',[event_id])
        posts = namedtuplefetchall(cursor)

        cursor.execute('''SELECT count(*) cnt FROM event_participants 
                       where event_id=%s and state_of_participation='going' ''',[event_id])
        going_number = namedtuplefetchall(cursor)[0].CNT
        cursor.execute('''SELECT count(*) cnt FROM event_participants 
                       where event_id=%s and state_of_participation='not going' ''',[event_id])
        not_going_number = namedtuplefetchall(cursor)[0].CNT
        cursor.execute('''SELECT count(*) cnt FROM event_participants 
                       where event_id=%s and state_of_participation='interested' ''',[event_id])
        interested_number = namedtuplefetchall(cursor)[0].CNT
        cursor.execute('''SELECT count(*) cnt FROM event_participants 
                        where event_id=%s and state_of_participation='invited' ''', [event_id])
        invited_number = namedtuplefetchall(cursor)[0].CNT

        cursor.execute('''SELECT * FROM event_table where event_id=%s''',[event_id])
        event_details = namedtuplefetchall(cursor)[0]
        cursor.execute('''update event_table SET going_number=%s, not_going_number=%s, interested_number=%s, 
                        invited_number=%s where event_id=%s''',
                       [going_number,not_going_number,interested_number,invited_number,event_id])
        args = {
            'event_id': event_id,
            'event_details': event_details,
            'posts': posts,
            'going_number': going_number,
            'not_going_number': not_going_number,
            'interested_number': interested_number,
            'invited_number': invited_number,
        }
    return render(request, template_name, args)


def view_all_events(request):
    template_name = 'events/event_list.html'
    with connection.cursor() as cursor:
        cursor.execute('''SELECT * FROM event_table ORDER BY event_id''')
        event_list = namedtuplefetchall(cursor)
        print(event_list)
        args = {
            'event_list': event_list
        }
    return render(request, template_name, args)


class CreateEventView(TemplateView):
    template_name = 'events/create_event.html'
    template_name2 = 'events/event_create_successful.html'

    def get(self, request):
        form = forms.CreateEventForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = forms.CreateEventForm(request.POST)
        if form.is_valid():
            event_name = form.cleaned_data['event_name']
            event_place = form.cleaned_data['event_place']
            description = form.cleaned_data['description']
            event_date_time = str(form.cleaned_data['event_date_time'])
            print(event_date_time)
            # insertion in database
            with connection.cursor() as cursor:
                cursor.execute('''insert into event_table(creator_id,event_date_time,event_place,description,event_name) 
                                  values(%s,%s,%s,%s,%s);''',
                                 [request.session['user_id'],event_date_time,event_place,description,event_name])
                cursor.execute('''select max(event_id) event_id from event_table''')
                event_id = namedtuplefetchall(cursor)[0].EVENT_ID
                args = {
                    'event_id':event_id
                }
        else:
            form = forms.CreateEventForm()
            return render(request, self.template_name, {'form': form})

        return render(request, self.template_name2, args)


def event_members(request, event_id):
    template_name = 'events/event_member_list.html'
    with connection.cursor() as cursor:
        cursor.execute('''select * from event_participants g join users u on(g.member_id=u.user_id) 
                        where event_id=%s ORDER BY u.username''',[event_id])
        members = namedtuplefetchall(cursor)
        print(members)
        args = {
            'members': members
        }
    return render(request, template_name, args)


def invite_member(request, event_id):
    template_name = 'events/invite_member.html'
    with connection.cursor() as cursor:
        cursor.execute('''select * from users where user_id not in 
                        (select member_id from event_participants where event_id=%s) ORDER BY username''',[event_id])
        users = namedtuplefetchall(cursor)
        args = {
            'event_id': event_id,
            'users': users,
        }
    return render(request, template_name, args)


def member_invited(request, event_id, user_id):
    template_name = 'events/member_invited.html'
    state_of_participation = 'invited'
    with connection.cursor() as cursor:
        cursor.execute('''insert into event_participants(event_id,member_id,state_of_participation) values(%s,%s,%s)''',
                       [event_id,user_id,state_of_participation])
        args = {
            'event_id': event_id,
        }
    return render(request, template_name, args)
"""
########


def messenger_home(request):
    template_name = 'messenger/messenger_home.html'
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    args={
        'user_id': request.session['user_id']
    }
    return render(request, template_name, args)


def inbox(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name='messenger/inbox.html'
    user_id=request.session['user_id']
    with connection.cursor() as cursor:
        """cursor.execute('''
            select participant_id,conversation_id,username from convo_participants join users on (participant_id=user_id) 
            where conversation_id in(select conversation_id from convo_participants where participant_id=%s)''',[user_id]) 
            """

        cursor.execute('''select p.conversation_id id from convo_participants p join conversation c
                        on(p.conversation_id=c.conversation_id) where participant_id=%s order by c.last_convo_time desc''',
                        [user_id])
        conversation_ids=namedtuplefetchall(cursor)
        conversation_infos=[]
        for i in range(len(conversation_ids)):
            cursor.execute(
                '''select participant_id,conversation_id,username from convo_participants join users on (participant_id=user_id) 
                where conversation_id=%s''',[conversation_ids[i].ID])
            conversation_infos.append(namedtuplefetchall(cursor))
        #print(conversation_infos)
        args={
            'length':len(conversation_ids),
            'convs':conversation_infos,
            'user_id':user_id,
            'conv_ids':conversation_ids,
        }
    return render(request, template_name, args)


class ConversationView(TemplateView):

    def get(self, request, conv_id):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')

        conv_id=int(conv_id)
        template_name='messenger/conversation.html'
        if not request.session.has_key('user_id'):
            return HttpResponse('<h1>ERROR(not logged in)<h1>') ## or render the 'you are not logged-in' page
        user_id=request.session['user_id']
        #print('{}-{}'.format((conv_id),(user_id)))
        with connection.cursor() as cursor:
            """cursor.execute('''select user_id from users where user_id in (select participant_id from convo_participants 
                            where conversation_id=%s and user_id=%s)''',
                           [conv_id,user_id])
            temp = namedtuplefetchall(cursor)"""
            ###
            temp = cursor.callfunc('conv_authorize_check_func', int, [conv_id, user_id])
            print('-->{}'.format(temp))

            if temp==0:
                print('not permitted.')
                return HttpResponse('<h1>ERROR(not authorized)<h1>')
            cursor.execute('''
                            select m.*, u1.username sender_name, u1.user_id id from message m 
                            join users u1 on(m.sender_id=u1.user_id) 
                            where conversation_id=%s order by send_time desc''', ## <<???
                           [conv_id])

            messages = namedtuplefetchall(cursor)
            #msgForm1 = forms.MessageFormText()
            #msgForm2 = forms.MessageFormMedia()
            msgForm = forms.MessageForm()

            """cursor.execute(''' select account_state from users where user_id=%s''',
                           [request.session['user_id']])
            state = namedtuplefetchall(cursor)[0].ACCOUNT_STATE
            print(state)
            if state != 'active':
                msgForm = None"""

            args={
                #'formText':msgForm1,
                #'formMedia':msgForm2,
                'form':msgForm,
                'user_id':user_id,
                'messages':messages,
                'convo_id':conv_id,
            }

            return render(request,template_name,args)

    def post(self, request, conv_id):
        #msgForm1 = forms.MessageFormText(request.POST)
        #msgForm2 = forms.MessageFormMedia(request.POST)
        msgForm = forms.MessageForm(request.POST)
        if not msgForm.is_valid():
            return HttpResponse('Message Error.')
        text = msgForm.cleaned_data['text']
        #print(text)
        #### media = msgForm.cleaned_data['text'] ### media handling
        conv_id=int(conv_id)
        template_name='messenger/conversation.html'
        if not request.session.has_key('user_id'):
            HttpResponse('ERROR(not logged in)') ## or render the 'you are not logged-in' page
        user_id=request.session['user_id']
        #print('{}-{}'.format((conv_id),(user_id)))
        with connection.cursor() as cursor:
            """???
            cursor.execute('''select user_id from users where user_id in (select participant_id from convo_participants 
                            where conversation_id=%s and user_id=%s)''',
                           [conv_id, user_id])
            ???
            temp = namedtuplefetchall(cursor)"""
            ###
            temp = cursor.callfunc('conv_authorize_check_func', int, [conv_id, user_id])
            print('cac_func: {}'.format(temp))

            if temp == 0:
                HttpResponse('ERROR(not authorized)')

            if text!='':
                """cursor.execute('''insert into message(conversation_id,sender_id,message_text)
                                values(%s,%s,%s)''',[conv_id,user_id,text])"""
                cursor.callproc('message_procedure', [conv_id,user_id,text])
            cursor.execute('''
                            select m.*, u1.username sender_name, u1.user_id id from message m 
                            join users u1 on(m.sender_id=u1.user_id) 
                            where conversation_id=%s order by send_time desc ''', ## <<???
                           [conv_id])

            messages = namedtuplefetchall(cursor)
            #msgForm1 = forms.MessageFormText()
            #msgForm2 = forms.MessageFormMedia()
            msgForm = forms.MessageForm()
            args = {
                #'formText': msgForm1,
                #'formMedia': msgForm2,
                'form':msgForm,
                'user_id': user_id,
                'messages': messages,
                'convo_id': conv_id,
            }

            return render(request,template_name,args)


def other_users(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name='messenger/other_users.html'
    if not request.session.has_key('user_id'):
        HttpResponse('ERROR(not logged in)')  ### or render the 'you are not logged-in' page
    user_id = request.session['user_id']

    with connection.cursor() as cursor:
        cursor.execute('''select * from users where user_id!=%s and account_state='active' order by online_check desc''',
                       [user_id])
        users = namedtuplefetchall(cursor)
        length = len(users)

        args = {
            'user_id': user_id,
            'users':users,
            'length':length,
        }
    return render(request, template_name, args)


def message_user(request,uid):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'messenger/message_user.html'
    if not request.session.has_key('user_id'):
        HttpResponse('ERROR(not logged in)')  ### or render the 'you are not logged-in' page
    user_id = request.session['user_id']
    member_number = 2
    with connection.cursor() as cursor:
        """???
        cursor.execute('''select c.CONVERSATION_ID id
                        from CONVERSATION c join CONVO_PARTICIPANTS p on(c.CONVERSATION_ID=p.CONVERSATION_ID)
                        where c.private_convo_check='y' and
                        ((convo_starter_id=%s and participant_id=%s) or (convo_starter_id=%s and participant_id=%s)) ''',
                       [user_id,uid,uid,user_id])
        conv_id = namedtuplefetchall(cursor)#[0].ID

        if conv_id == []:

            cursor.execute(''' insert into conversation(convo_starter_id,member_number,private_convo_check) 
                            values(%s,2,'y') ''',[user_id])
            cursor.execute('''select max(CONVERSATION_ID) id from conversation ''')
            conv_id = namedtuplefetchall(cursor)[0].ID
            cursor.execute(''' insert into convo_participants(conversation_id,participant_id,online_check) 
                            values(%s,%s,'n') ''',[conv_id,user_id])
            cursor.execute(''' insert into convo_participants(conversation_id,participant_id,online_check) 
                            values(%s,%s,'n') ''',[conv_id,uid])
        else:
            conv_id = conv_id[0].ID
        ???"""
        conv_id=cursor.callfunc('one_to_one_convo_participants', int, [user_id, uid])
        print('c_id: {}'.format(conv_id))

        args = {
            'user_id': user_id,
            'convo_id': conv_id,
            'uid': uid,
        }
    return render(request,template_name,args)


class StartGroupConvoView(TemplateView):
    def get(self, request):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')  ### or render the 'you are not logged-in' page

        template_name = 'messenger/start_group_convo.html'
        user_id = request.session['user_id']
        with connection.cursor() as cursor:
            cursor.execute('''select * from users where user_id!=%s and account_state='active' order by username''',[user_id])
            users = namedtuplefetchall(cursor)
            form = forms.GroupConvoForm(users=users)
            #print(users[0].USERNAME)
            args = {
                'user_id': user_id,
                'users': users,
                'form': form
            }
        return render(request, template_name, args)

    def post(self, request):
        if not request.session.has_key('user_id'):
            return render(request, 'user_account/goto_login.html')  ### or render the 'you are not logged-in' page

        template_name = 'messenger/conv_created.html'
        user_id = request.session['user_id']
        member_number = 1
        with connection.cursor() as cursor:
            cursor.execute('''select * from users where user_id!=%s order by username''',[user_id])
            users = namedtuplefetchall(cursor)
            #users = None
            form = forms.GroupConvoForm(request.POST,users=users)
            if not form.is_valid():
                HttpResponse('ERROR(form error)')
            members = form.cleaned_data['members']
            #print('members: {}'.format(members))
            member_number += len(members)
            cursor.execute(''' insert into conversation(convo_starter_id,member_number,private_convo_check) 
                               values(%s,%s,'n') ''', [user_id,member_number])
            cursor.execute('''select max(CONVERSATION_ID) id from conversation ''')
            conv_id = namedtuplefetchall(cursor)[0].ID
            cursor.execute(''' insert into convo_participants(conversation_id,participant_id,online_check) 
                               values(%s,%s,'n') ''', [conv_id, user_id])
            for id in members:
                cursor.execute(''' insert into convo_participants(conversation_id,participant_id,online_check) 
                                    values(%s,%s,'n') ''', [conv_id, id])
            args = {
                'convo_id': conv_id,
            }
        return render(request,template_name,args)