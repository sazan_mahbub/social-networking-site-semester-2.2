from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'^', include('test_python_hol.urls')),
    #url(r'^blog/', include('blog.urls')),
    url(r'^user_posts/', include('test2.urls')),
    url(r'^groups/', include('groups.urls')),
    url(r'^events/', include('events.urls')),
    url(r'^user_account/', include('user_account.urls')),
    url(r'^messenger/', include('messenger.urls')),
    url(r'^search/', include('search_app.urls')),

]


#superuser:
#user name: sazan or admin
#password: 123456789 or abcde12345 or 12345abcde