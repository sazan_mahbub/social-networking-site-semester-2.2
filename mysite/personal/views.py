from django.shortcuts import render

def index(request):
    return render(request, 'test_python_hol/post.html')

def contact(request):
    return render(request, 'test_python_hol/basic.html', {'content':['email me', 'a@b.com']})
