from django import forms
#from . models import Post
from datetime import datetime


class MessageFormMedia(forms.Form):
    media = forms.CharField(label='media')


class MessageFormText(forms.Form):
    text = forms.CharField(label='text')


class MessageForm(forms.Form):
    #media = forms.CharField(label='media', required=False)
    text = forms.CharField(label='text')


#react_choices = [['None','None'],['like','like'],['dislike','dislike']]
class GroupConvoForm(forms.Form):
    #members = None

    def __init__(self, *args, **kwargs):
        temp = kwargs.pop('users')
        super(GroupConvoForm, self).__init__(*args, **kwargs)
        self.fields['members'] = forms.MultipleChoiceField(label="members",
                                                           choices=[[t.USER_ID, t.USERNAME] for t in temp],
                                                           widget=forms.CheckboxSelectMultiple)


class SearchForm(forms.Form):
    search = forms.CharField(label='search')



class AdvancedSearchForm(forms.Form):
    _from = forms.DateTimeField(widget=forms.SelectDateWidget(years=range(1900, 2019)),
                                        initial=datetime.today().replace(month=1, day=1, year=2017))
    _to = forms.DateTimeField(widget=forms.SelectDateWidget(years=range(1900, 2019)),
                                        initial=datetime.today())