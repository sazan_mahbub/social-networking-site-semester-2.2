from django.conf.urls import url, include
from django.contrib.auth.views import login
from django.contrib import admin
from search_app import views

urlpatterns = [
    url(r'^$', views.normal_search, name='normal_search'),
    url(r'^advanced_search/$', views.advanced_search, name='advanced_search'),

    ###
    #url(r'^inbox/$', views.inbox, name='inbox'),
    #url(r'^other_users/$', views.other_users, name='other_users'),
    #url(r'^message_user/(?P<uid>\d+)/$', views.message_user, name='message_user'),
    #url(r'^start_group_convo/$', views.StartGroupConvoView.as_view(), name='start_group_convo'),
    #url(r'^conversation/(?P<conv_id>\d+)/$', views.ConversationView.as_view(), name='conversation'),
    ####
]
