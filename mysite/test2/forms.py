from django import forms
#from . models import Post

'''
class HomeModelForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = [
                'post_text',
                'posted_by_id',
                'posted_on_id',
                ''
                ]
'''

class Homeform(forms.Form):
    post_id = forms.IntegerField()
    post = forms.CharField()
    posted_by = forms.IntegerField()
    posted_on = forms.IntegerField()
    privacy = forms.CharField()

#privacy_choices = [['private','private'],['public','public'],['friends','friends'],['f_of_f','friends of friends'],['only_me','only me'],['custom','custom']]
privacy_choices = [['friends','friends'],['public','public'],['f_of_f','friends of friends'],['only_me','only me'],['custom','custom']]
class PostForm(forms.Form):
    post = forms.CharField(label='Post', widget=forms.Textarea)
    #posted_by = forms.IntegerField() # this needs to be gone
    #posted_on = forms.IntegerField()
    privacy = forms.ChoiceField(choices=privacy_choices)


class CommentForm(forms.Form):
    comment = forms.CharField(label='comment')


react_choices = [['None','None'],['like','like'],['dislike','dislike']]
class ReactForm(forms.Form):
    react = forms.ChoiceField(choices=react_choices)

