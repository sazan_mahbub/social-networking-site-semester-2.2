from django.conf.urls import url, include
from django.contrib.auth.views import login
from test2.views import HomeView, PostView
from django.contrib import admin
from test2 import views

urlpatterns = [
    #url(r'^$', HomeView.as_view()),
    url(r'^all_posts/$', views.view_all_posts, name='all_posts'),
    url(r'^(?P<user_id>\d+)/post/$', views.PostView.as_view(), name='post'),
    url(r'^all_posts/(?P<post_id>\d+)/$', views.PostDetailsView.as_view(), name='post_details'),
    #url(r'^post_comment/$', views.post_comment, name='comment'),
    #url(r'^post_react/$', views.post_react, name='react'),
]
