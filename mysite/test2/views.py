from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import TemplateView
from test2.forms import Homeform, PostForm
from . import models
from django.db import connection, transaction
from collections import namedtuple
import datetime
from test2 import forms
import cx_Oracle


# Create your views here.

class HomeView(TemplateView):
    template_name = 'test2/post.html'
    template_name2 = 'test2/posts_page.html'

    def get(self, request):
        form = Homeform()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = Homeform(request.POST)
        if form.is_valid():
            post_id = form.cleaned_data['post_id']
            post = form.cleaned_data['post']
            posted_by = form.cleaned_data['posted_by']
            posted_on = form.cleaned_data['posted_on']
            privacy = form.cleaned_data['privacy']

            # post_time = datetime.datetime.now()

            form = Homeform()
            # return redirect('home:home')
            # users = self.my_custom_sql()
            # posted_by =self.get_user(posted_by)[0]
            # posted_on =self.get_user(posted_on)[0]

            # insertion in database
            with connection.cursor() as cursor:
                cursor.execute('''INSERT INTO post(posted_by_id, posted_on_id, post_text, privacy) VALUES (
                %s, %s, %s, %s)''', [posted_by, posted_on, post, privacy])
                cursor.execute('''SELECT * FROM POST''')
                posts = self.namedtuplefetchall(cursor)

        # args = {'post_id':post_id, 'post':post, 'posted_by':posted_by, 'posted_on':posted_on, 'privacy':privacy, 'post_time':post_time}
        args = {'posts': posts}
        return render(request, self.template_name2, args)

    def my_custom_sql(self):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT * FROM users''')
            row = self.namedtuplefetchall(cursor)

        for i in row:
            print(i.USERNAME)
        return row

    def get_user(self, u_id):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT * FROM users where user_id = %s''', [u_id])
            row = self.namedtuplefetchall(cursor)

        for i in row:
            print(i.USERNAME)
        return row

    def namedtuplefetchall(self, cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


class PostView(TemplateView):
    template_name = 'test2/post.html'
    template_name2 = 'test2/post_successful.html'

    def get(self, request, user_id):
        form = PostForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request, user_id):
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.cleaned_data['post']
            posted_by = request.session['user_id']
            posted_on = user_id
            privacy = form.cleaned_data['privacy']
            # insertion in database
            with connection.cursor() as cursor:
                cursor.callproc('post_insertion', [posted_by, posted_on, post, privacy])
                cursor.execute('''select u1.user_id u1_id, u1.username u1name, u2.user_id u2_id ,u2.username u2name, p.*
                                  from post p join users u1 on(p.posted_by_id=u1.user_id)
                                  join users u2 on(p.posted_on_id=u2.user_id)
                                  where post_id = (select max(post_id) from post)''')
                post = namedtuplefetchall(cursor)[0]
                args = {
                    'post': post
                }
        else:
            form = PostForm()
            return render(request, self.template_name, {'form': form})

        return render(request, self.template_name2, args)

    def my_custom_sql(self):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT * FROM users''')
            row = namedtuplefetchall(cursor)

        for i in row:
            print(i.USERNAME)
        return row

    def get_user(self, u_id):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT * FROM users where user_id = %s''', [u_id])
            row = namedtuplefetchall(cursor)

        for i in row:
            print(i.USERNAME)
        return row


class PostDetailsView(TemplateView):
    template_name = 'test2/post_details.html'
    post_id = None
    post_details = None
    comment_form = None
    react_form = None
    args = None

    def get(self, request, post_id):
        with connection.cursor() as cursor:
            cursor.execute('''select u1.user_id u1_id, u1.username u1name, u2.user_id u2_id ,u2.username u2name, p.*
                              from post p join users u1 on(p.posted_by_id=u1.user_id)
                              join users u2 on(p.posted_on_id=u2.user_id)
                              where post_id = %s''', [post_id])
            self.post_id = post_id
            self.post_details = namedtuplefetchall(cursor)[0]
            self.comment_form = forms.CommentForm()
            self.react_form = forms.ReactForm()

            cursor.execute('''select count(*) cnt from user_post_reaction where post_id=%s and react='like' ''',
                           [post_id])
            like_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select count(*) cnt from user_post_reaction where post_id=%s and react='dislike' ''',
                           [post_id])
            dislike_count = namedtuplefetchall(cursor)[0].CNT
            cursor.execute('''select count(*) cnt from user_post_comment where post_id=%s
                              and comment_text is not NULL ''',
                           [post_id])
            comment_count = namedtuplefetchall(cursor)[0].CNT

            """
            like_count = cursor.var(cx_Oracle.NUMBER)
            dislike_count = cursor.var(cx_Oracle.NUMBER)
            comment_count = cursor.var(cx_Oracle.NUMBER)
            cursor.callproc('post_react_cmnt_count',[post_id,like_count,dislike_count,comment_count])"""

            print('>>>{},{},{}'.format(like_count,dislike_count,comment_count))
            cursor.execute('''update post set like_count=%s,dislike_count=%s,comment_count=%s where post_id=%s''',
                           [like_count,dislike_count,comment_count,post_id])

            cursor.execute('''select u.username, r.comment_text from user_post_comment r 
                            join users u on(r.user_id=u.user_id) 
                            where r.post_id=%s order by r.user_post_comment_id desc''',
                           [post_id])
            all_comments = namedtuplefetchall(cursor)
            self.args = {
                'post_id': self.post_id,
                'post': self.post_details,
                'comment_form': self.comment_form,
                'react_form': self.react_form,
                'like_count': like_count,
                'dislike_count': dislike_count,
                'comment_count': comment_count,
                'all_comments': all_comments,
            }
            #print(all_comments)
        return render(request, self.template_name, self.args)

    def post(self, request, post_id):
        #print(self.args)
        comment_form = forms.CommentForm(request.POST)
        react_form = forms.ReactForm(request.POST)
        if comment_form.is_valid() or react_form.is_valid():
            comment = ''
            react  = ''
            if comment_form.is_valid():
                comment = comment_form.cleaned_data['comment']
            if react_form.is_valid():
                react = react_form.cleaned_data['react']
            commented_by = request.session['user_id']
            print('{}-{}-{}'.format(comment, react, commented_by))
            # handling database
            with connection.cursor() as cursor:
                try:
                    if react != '':
                        cursor.execute('''insert into user_post_reaction(user_id, post_id, react) 
                                      values(%s,%s,%s)''', [request.session['user_id'], post_id, react])
                    elif comment != '':
                        cursor.execute('''insert into user_post_comment(user_id, post_id, comment_text) 
                                      values(%s,%s,%s)''', [request.session['user_id'], post_id, comment])
                except:
                    if react!='':
                        if react == "None": react = ''
                        cursor.execute('''update user_post_reaction set react=%s 
                                          where user_id=%s and post_id=%s''',
                                       [react, request.session['user_id'], post_id])
                    """elif comment!='':
                        cursor.execute('''update user_post_comment set comment_text=%s 
                                          where user_id=%s and post_id=%s''',
                                          [comment, request.session['user_id'], post_id])"""

                    print('already in hoss!')
                cursor.execute('''select u1.user_id u1_id, u1.username u1name, u2.user_id u2_id ,u2.username u2name, p.*
                                  from post p join users u1 on(p.posted_by_id=u1.user_id)
                                  join users u2 on(p.posted_on_id=u2.user_id)
                                  where post_id = %s''', [post_id])
                self.post_id = post_id
                self.post_details = namedtuplefetchall(cursor)[0]
                self.comment_form = forms.CommentForm()
                self.react_form = forms.ReactForm()
                cursor.execute('''select count(*) cnt from user_post_reaction where post_id=%s and react='like' ''',
                                  [post_id])
                like_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''select count(*) cnt from user_post_reaction where post_id=%s and react='dislike' ''',
                                  [post_id])
                dislike_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''select count(*) cnt from user_post_comment where post_id=%s
                                  and comment_text is not NULL ''',
                                  [post_id])
                comment_count = namedtuplefetchall(cursor)[0].CNT
                cursor.execute('''update post set like_count=%s,dislike_count=%s,comment_count=%s where post_id=%s''',
                               [like_count, dislike_count, comment_count, post_id])
                cursor.execute('''select u.username, r.comment_text from user_post_comment r 
                                join users u on(r.user_id=u.user_id) 
                                where r.post_id=%s order by r.user_post_comment_id desc''',
                               [post_id])
                all_comments = namedtuplefetchall(cursor)
                self.args = {
                    'post_id': self.post_id,
                    'post': self.post_details,
                    'comment_form': self.comment_form,
                    'react_form': self.react_form,
                    'like_count': like_count,
                    'dislike_count': dislike_count,
                    'comment_count': comment_count,
                    'all_comments': all_comments,
                }
            return render(request, self.template_name, self.args)
        else:
            pass


def view_all_posts(request):
    template_name = 'test2/posts_page.html'
    with connection.cursor() as cursor:
        cursor.execute('''SELECT * FROM post ORDER BY post_id DESC''')
        posts = namedtuplefetchall(cursor)
        # print(posts)
        args = {
            'posts': posts
        }
    return render(request, template_name, args)


def post_comment(request):
    if request.method == 'POST':
        comment = request.POST.get('comment')
        print('{}-{}'.format(comment, request.session['user_id']))
    return HttpResponse('')


def post_react(request):
    if request.method == 'POST':
        print('liked by {}'.format(request.session['user_id']))
    return HttpResponse('')
