from django.contrib import admin

# Register your models here.
from test_python_hol import models

admin.site.register(models.Conversation)
admin.site.register(models.ConvoParticipants)
admin.site.register(models.Department)
admin.site.register(models.Post)
admin.site.register(models.DjangoMigrations)
admin.site.register(models.Employee)
admin.site.register(models.EventParticipants)
admin.site.register(models.EventPost)
admin.site.register(models.EventPostReaction)
admin.site.register(models.EventTable)
admin.site.register(models.GroupMember)
admin.site.register(models.GroupPost)
admin.site.register(models.GroupPostReaction)
admin.site.register(models.Groups)
admin.site.register(models.Media)
admin.site.register(models.Message)
admin.site.register(models.UserPostReaction)
admin.site.register(models.Users)
admin.site.register(models.Relation)


