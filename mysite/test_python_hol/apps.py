from django.apps import AppConfig


class TestPythonHolConfig(AppConfig):
    name = 'test_python_hol'
