# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Conversation(models.Model):
    conversation_id = models.BigIntegerField(primary_key=True)
    convo_starter = models.ForeignKey('Users', models.DO_NOTHING)
    start_time = models.DateField()
    last_convo_time = models.DateField()
    member_number = models.BigIntegerField()
    private_convo_check = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'conversation'


class ConvoParticipants(models.Model):
    conversation = models.ForeignKey(Conversation, models.DO_NOTHING, primary_key=True)
    participant = models.ForeignKey('Users', models.DO_NOTHING) #?????
    join_time = models.DateField()
    online_check = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'convo_participants'
        unique_together = (('conversation', 'participant'),)


class Department(models.Model):
    dept_id = models.BigIntegerField(primary_key=True)
    dept_name = models.CharField(unique=True, max_length=20)
    member_number = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'department'


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Employee(models.Model):
    employee_id = models.BigIntegerField(primary_key=True)
    employee_name = models.CharField(max_length=30)
    date_of_birth = models.DateField()
    monthly_salary = models.FloatField()
    dept_name = models.ForeignKey(Department, models.DO_NOTHING, db_column='dept_name', null=True)
    manager = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'employee'


class EventParticipants(models.Model):
    event = models.ForeignKey('EventTable', models.DO_NOTHING, primary_key=True)
    member = models.ForeignKey('Users', models.DO_NOTHING)
    state_of_participation = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'event_participants'
        unique_together = (('event', 'member'),)


class EventPost(models.Model):
    event_post_id = models.BigIntegerField(primary_key=True)
    event = models.ForeignKey('EventTable', models.DO_NOTHING)
    posted_by = models.ForeignKey('Users', models.DO_NOTHING)
    event_post_text = models.CharField(max_length=1000, blank=True, null=True)
    media = models.ForeignKey('Media', models.DO_NOTHING, blank=True, null=True)
    like_count = models.BigIntegerField()
    dislike_count = models.BigIntegerField()
    comment_count = models.BigIntegerField()
    event_post_time = models.DateField()
    ip_address = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'event_post'


class EventPostReaction(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, primary_key=True)
    event_post = models.ForeignKey(EventPost, models.DO_NOTHING)
    react = models.CharField(max_length=10, blank=True, null=True)
    comment_text = models.CharField(max_length=300, blank=True, null=True)
    media = models.ForeignKey('Media', models.DO_NOTHING, blank=True, null=True)
    reaction_time = models.DateField()

    class Meta:
        managed = False
        db_table = 'event_post_reaction'
        unique_together = (('user', 'event_post'),)


class EventTable(models.Model):
    event_id = models.BigIntegerField(primary_key=True)
    creator = models.ForeignKey('Users', models.DO_NOTHING)
    member_number = models.BigIntegerField()
    create_date = models.DateField()
    event_date_time = models.DateField()
    event_place = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True)
    going_number = models.BigIntegerField()
    not_going_number = models.BigIntegerField()
    interested_number = models.BigIntegerField()
    invited_number = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'event_table'


class GroupMember(models.Model):
    group = models.ForeignKey('Groups', models.DO_NOTHING, primary_key=True)
    member = models.ForeignKey('Users', models.DO_NOTHING)
    admin = models.CharField(max_length=1, blank=True, null=True)
    join_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'group_member'
        unique_together = (('group', 'member'),)


class GroupPost(models.Model):
    group_post_id = models.BigIntegerField(primary_key=True)
    group = models.ForeignKey('Groups', models.DO_NOTHING)
    posted_by = models.ForeignKey('Users', models.DO_NOTHING)
    group_post_text = models.CharField(max_length=1000, blank=True, null=True)
    media = models.ForeignKey('Media', models.DO_NOTHING, blank=True, null=True)
    like_count = models.BigIntegerField()
    dislike_count = models.BigIntegerField()
    comment_count = models.BigIntegerField()
    group_post_time = models.DateField()
    ip_address = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_post'


class GroupPostReaction(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, primary_key=True)
    group_post = models.ForeignKey(GroupPost, models.DO_NOTHING)
    react = models.CharField(max_length=10, blank=True, null=True)
    comment_text = models.CharField(max_length=300, blank=True, null=True)
    media = models.ForeignKey('Media', models.DO_NOTHING, blank=True, null=True)
    reaction_time = models.DateField()

    class Meta:
        managed = False
        db_table = 'group_post_reaction'
        unique_together = (('user', 'group_post'),)


class Groups(models.Model):
    group_id = models.BigIntegerField(primary_key=True)
    creator = models.ForeignKey('Users', models.DO_NOTHING)
    member_number = models.BigIntegerField()
    create_date = models.DateField()
    description = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'groups'


class Media(models.Model):
    media_id = models.BigIntegerField(primary_key=True)
    media_type = models.CharField(max_length=7, blank=True, null=True)
    media_file_location = models.CharField(max_length=200, blank=True, null=True)
    last_modify_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'media'


class Message(models.Model):
    message_id = models.BigIntegerField(primary_key=True)
    conversation = models.ForeignKey(Conversation, models.DO_NOTHING)
    sender = models.ForeignKey('Users', models.DO_NOTHING)
    send_time = models.DateField()
    message_text = models.CharField(max_length=300, blank=True, null=True)
    media = models.ForeignKey(Media, models.DO_NOTHING, blank=True, null=True)
    sender_ip_address = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'message'


class Post(models.Model):
    post_id = models.BigIntegerField(primary_key=True)
    posted_by = models.ForeignKey('Users', models.DO_NOTHING, related_name='related_posted_by_relation', unique=True)
    posted_on = models.ForeignKey('Users', models.DO_NOTHING, related_name='related_posted_on_relation', unique=True)
    post_text = models.CharField(max_length=1000, blank=True, null=True)
    media = models.ForeignKey(Media, models.DO_NOTHING, blank=True, null=True)
    like_count = models.BigIntegerField()
    dislike_count = models.BigIntegerField()
    comment_count = models.BigIntegerField()
    post_time = models.DateField()
    ip_address = models.CharField(max_length=50, blank=True, null=True)
    privacy = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'post'


class Relation(models.Model):
    sender = models.ForeignKey('Users', models.DO_NOTHING, related_name='related_sender_relation', primary_key=True)
    receiver = models.ForeignKey('Users', models.DO_NOTHING, related_name='related_receiver_relation')
    first_contact_date_time = models.DateField()
    last_contact_date_time = models.DateField(blank=True, null=True)
    state_of_relation = models.CharField(max_length=7)

    class Meta:
        managed = False
        db_table = 'relation'
        unique_together = (('sender', 'receiver'),)


class UserPostReaction(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, primary_key=True)
    post = models.ForeignKey(Post, models.DO_NOTHING)
    react = models.CharField(max_length=10, blank=True, null=True)
    comment_text = models.CharField(max_length=300, blank=True, null=True)
    media = models.ForeignKey(Media, models.DO_NOTHING, blank=True, null=True)
    reaction_time = models.DateField()

    class Meta:
        managed = False
        db_table = 'user_post_reaction'
        unique_together = (('user', 'post'),)


class Users(models.Model):
    user_id = models.BigIntegerField(primary_key=True)
    username = models.CharField(unique=True, max_length=30)
    password = models.CharField(max_length=50)
    email = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20, blank=True, null=True)
    gender = models.CharField(max_length=10, blank=True, null=True)
    date_of_birth = models.DateField()
    nationality = models.CharField(max_length=20)
    relationship_status = models.CharField(max_length=10, blank=True, null=True)
    language = models.CharField(max_length=20, blank=True, null=True)
    occupation = models.CharField(max_length=20, blank=True, null=True)
    education_current = models.CharField(max_length=20, blank=True, null=True)
    profile_pic = models.ForeignKey(Media, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
