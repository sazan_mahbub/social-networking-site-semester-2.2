from django import forms
from datetime import datetime
from django.contrib.admin.widgets import AdminDateWidget

rel_choices = [['', 'None'],['single', 'single'], ['engaged', 'engaged'], ['married', 'married'], ['unmarried', 'unmarried'],
               ['divorced', 'divorced'], ['widowed', 'widowed'], ['others', 'others']]


class RegistrationForm(forms.Form):
    # user_id = forms.IntegerField() autoincrement, input dewa lagbe na
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    email = forms.CharField(widget=forms.EmailInput)
    first_name = forms.CharField()
    last_name = forms.CharField(required=False)
    gender = forms.ChoiceField(widget=forms.RadioSelect, choices=[['male', 'male'], ['female', 'female']],
                               required=False)
    date_of_birth = forms.DateTimeField(widget = forms.SelectDateWidget(years=range(1900, 2019)),
                                        initial=datetime.today().replace(month=1,day=1,year=1990))
                                        #initial=datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p').date())
                                        #initial=parser.parse("Jan 1 1900 12:00AM"))
    nationality = forms.CharField()
    relationship_status = forms.ChoiceField(choices=rel_choices, required=False)
    language = forms.CharField(required=False)
    occupation = forms.CharField(required=False)
    education_current = forms.CharField(required=False)
    profile_pic = forms.FileField(label="Select a profile image", required=False)


class LoginForm(forms.Form):
    username_or_email = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


