from django.conf.urls import url, include
from django.contrib.auth.views import login
from user_account import views
from user_account.views import RegistrationView, LoginView, AccountView, user_list, friend_list
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^registration/$', RegistrationView.as_view(), name='registration'),
    url(r'^$', LoginView.as_view(), name='login'),
    url(r'^(?P<user_id>\d+)/account_details/$', views.account_details, name='account_details'),
    url(r'^(?P<user_id>\d+)/add_friend/$', views.add_friend, name='add_friend'),
    url(r'^user_list/$', user_list, name='user_list'),
    url(r'^(?P<user_id>\d+)/related_people/$', views.other_related_people, name='related_people'),
    url(r'^(?P<user_id>\d+)/friend_list/$', friend_list, name='friend_list'),
    url(r'^(?P<user_id>\d+)/request_accept/$', views.request_accept, name='request_accept'),
    url(r'^(?P<user_id>\d+)/request_reject/$', views.request_reject, name='request_reject'),
    url(r'^(?P<user_id>\d+)/delete_friend/$', views.delete_friend, name='delete_friend'),
    url(r'^friend_request/$', views.friend_request, name='friend_request'),
    url(r'^connectivity/$', views.relation_home, name='relation_home'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^home/$', views.homepage, name='home'),
    url(r'^deactivated/$', views.deactivate_account, name='deactivate_account'),
    url(r'^notification/$', views.notification, name='notification'),
    #url(r'^list/$', friend_list, name='friend_list'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    #urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
