from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import TemplateView
from user_account.forms import RegistrationForm, LoginForm
from django.db import connection, transaction
from collections import namedtuple
from test2.models import Users
from django.template import loader
from django.core.files.storage import FileSystemStorage
#from PIL import Image
#from settings import MEDIA_URL


def handle_upload_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


class RegistrationView(TemplateView):
    template_name = 'user_account/registration_page.html'
    template_name2 = 'user_account/registration_success.html'

    def get(self, request):
        form = RegistrationForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = RegistrationForm(request.POST, request.FILES)

        if form.is_valid():
            uploaded_file_url = ''
            try:
                myfile = request.FILES['profile_pic']
                #handle_upload_file(myfile)
                fs = FileSystemStorage(
                        location=r'C:\Users\Sazan\Desktop\DB Project tests\django_sentdex\mysite\media',
                        base_url=r'C:\Users\Sazan\Desktop\DB Project tests\django_sentdex\mysite\media'
                        )
                filename = fs.save(myfile.name, myfile)
                #print(filename)
                uploaded_file_url = fs.url(filename)
                print(uploaded_file_url)
            except:
                print('File Not found')
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            gender = form.cleaned_data['gender']
            date_of_birth = form.cleaned_data['date_of_birth']
            nationality = form.cleaned_data['nationality']
            relationship_status = form.cleaned_data['relationship_status']
            language = form.cleaned_data['language']
            occupation = form.cleaned_data['occupation']
            education_current = form.cleaned_data['education_current']
            # insertion in database
            with connection.cursor() as cursor:
                cursor.execute('''insert into media(media_type,media_file_location)
                                values('picture',%s)''',[uploaded_file_url])
                cursor.execute('''select max(media_id) id from media''')
                pp_id = namedtuplefetchall(cursor)[0].ID
                cursor.execute('''insert into users(username, password, email, first_name, last_name, gender, 
                                date_of_birth, nationality, relationship_status, language, occupation, education_current,
                                profile_pic_id, account_state, online_check)
                                values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,'active','offline')''',
                               [username, password, email, first_name, last_name, gender, date_of_birth, nationality,
                                relationship_status, language, occupation, education_current, pp_id])
                cursor.execute('''SELECT * FROM USERS''')
                users = self.namedtuplefetchall(cursor)
                # transaction.commit()
                args = {'users': users}
            return render(request, self.template_name2, args)
        else:
            return HttpResponse('Error')

    def my_custom_sql(self):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT * FROM users''')
            row = self.namedtuplefetchall(cursor)

        for i in row:
            print(i.USERNAME)
        return row

    def get_user(self, u_id):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT * FROM users where user_id = %s''', [u_id])
            row = self.namedtuplefetchall(cursor)

        for i in row:
            print(i.USERNAME)
        return row

    def namedtuplefetchall(self, cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


class LoginView(TemplateView):
    template_name = 'user_account/login.html'
    template_name2 = 'user_account/login_success.html'
    template_name3 = 'user_account/login_failure.html'
    template_home = 'user_account/home.html'

    def get(self, request):
        if request.session.has_key('user_id'):
            # print('user_id: {}'.format(request.session['user_id']))
            user_id = request.session['user_id']
            return render(request, self.template_home, {'USER_ID': user_id})
        form = LoginForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = LoginForm(request.POST)
        login_check = 'Login Failed.'
        if form.is_valid():
            username_or_email = form.cleaned_data['username_or_email']
            password = form.cleaned_data['password']

            # database query for checking:
            with connection.cursor() as cursor:
                cursor.execute(''' select * from users
                                    where username = %s or email = %s ''',
                               [username_or_email, username_or_email])
                pw = self.namedtuplefetchall(cursor)
                if pw != []:
                    if pw[0].PASSWORD == password:  # always use capitals to call database objects
                        login_check = 'Login Successful.'
                        request.session['user_id'] = pw[0].USER_ID  ### starts a session...
                        cursor.execute(''' update users set online_check='online' where user_id=%s ''',
                                       [request.session['user_id']])
                        args = {
                            'logincheck': login_check,
                            'user_info': pw[0]
                        }

        if login_check == 'Login Failed.':
            return render(request, self.template_name3)

        return render(request, self.template_name2, args)

    def namedtuplefetchall(self, cursor):
        """Return all rows from a cursor as a namedtuple"""
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


class AccountView(TemplateView):
    template_name = 'account_details.html'

    def get(self, request):
        if not request.session.has_key('user_id'):
            render(request, 'user_account/goto_login.html')  ### or render the 'you are not logged-in' page

        self.user_id = request.GET.get('user_id', '')
        with connection.cursor() as cursor:
            cursor.execute(''' select * from users
                                where user_id = %s ''', [self.user_id])
        user_details = namedtuplefetchall(cursor)[0]
        args = {'user_details': user_details}
        return render(request, self.template_name, args)

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            username_or_email = form.cleaned_data['username_or_email']
            password = form.cleaned_data['password']

            # database query for checking:
            with connection.cursor() as cursor:
                cursor.execute(''' select * from users
                                        where username = %s or email = %s ''',
                               [username_or_email, username_or_email])
                pw = namedtuplefetchall(cursor)
                # print(pw)
                login_check = 'Login Failed.'
                args = {'logincheck': login_check}
                if pw != []:
                    if pw[0].PASSWORD == password:  # always use capitals to call database objects
                        login_check = 'Login Successful.'
                        args = {'logincheck': login_check, 'user_info': pw[0]}

        return render(request, self.template_name2, args)


def user_list(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')  ### or render the 'you are not logged-in' page

    all_users = Users.objects.all()
    template_name = 'user_account/users_list.html'
    # template = loader.get_template('user_account/users_list.html') # needed for 'return HttpResponse'
    args = {
        'all_users': all_users
    }
    return render(request, template_name, args)
    # return HttpResponse(template.render(args, request))


def friend_list(request, user_id=None):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')  ### or render the 'you are not logged-in' page

    relation = 'friend'
    with connection.cursor() as cursor:
        cursor.execute('''select * from users u1
                            where u1.user_id in ( select RECEIVER_ID from relation
                                                where SENDER_ID=%s and STATE_OF_RELATION=%s)
                               or u1.user_id in ( select SENDER_ID from relation
                                                where RECEIVER_ID=%s and STATE_OF_RELATION=%s)
                            ORDER BY u1.username''', [user_id, relation, user_id, relation])
        all_users = namedtuplefetchall(cursor)
        # print(all_users[0].USERNAME)

    template_name = 'user_account/user_list_show.html'
    args = {
        'list_title': 'friend list',
        'all_users': all_users
    }
    return render(request, template_name, args)


def account_details(request, user_id=None):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')  ### or render the 'you are not logged-in' page

    template_name = 'user_account/account_details.html'
    my_id = request.session['user_id']
    with connection.cursor() as cursor:
        cursor.execute(''' select * from users u join media m on(m.media_id=u.profile_pic_id)
                        where user_id = %s ''', [user_id])
        user_details = namedtuplefetchall(cursor)[0]
        cursor.execute('''select state_of_relation r from relation where (sender_id=%s and receiver_id=%s)
                        or (receiver_id=%s and sender_id=%s)''', [user_id, my_id, user_id, my_id])
        relation = namedtuplefetchall(cursor)
        if relation != []:
            relation = relation[0].R

        #img_src = "file://C:/Users/Sazan/Desktop/DB Project tests/django_sentdex/mysite/media/"+user_details.MEDIA_FILE_LOCATION
        #print(img_src)
        args = {
            'img_src': '',
            'my_id': my_id,
            'user_details': user_details,
            'relation': relation,
        }
    return render(request, template_name, args)


def logout(request):
    #print(request.session.has_key('user_id'))
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')  ### or render the 'you are not logged-in' page

    template_name = 'user_account/logout.html'
    with connection.cursor() as cursor:
        cursor.execute(''' update users set online_check='offline' where user_id=%s ''',
                       [request.session['user_id']])
    try:
        del request.session['user_id']
    except:
        pass
    return render(request, template_name)


def homepage(request):
    template_home = 'user_account/home.html'
    template_goto_login = 'user_account/goto_login.html'

    if request.session.has_key('user_id'):
        # print('user_id: {}'.format(request.session['user_id']))
        user_id = request.session['user_id']
        return render(request, template_home, {'USER_ID': user_id})
    return render(request, template_goto_login)


def add_friend(request, user_id):
    template_name = 'user_account/request_sent.html'
    my_id = request.session['user_id']
    with connection.cursor() as cursor:
        cursor.execute(''' insert into relation(sender_id,receiver_id,state_of_relation) 
                        values(%s,%s,%s)''', [my_id, user_id, 'follow'])
        args = {
            'my_id': my_id,
            'u_id': user_id
        }
    return render(request, template_name, args)


def other_related_people(request, user_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'user_account/related_people.html'
    with connection.cursor() as cursor:
        cursor.execute(''' select * from users u join relation r on (u.user_id=r.receiver_id) 
                        where sender_id=%s and state_of_relation = 'follow' ''',
                       [user_id])
        following_list = namedtuplefetchall(cursor)
        cursor.execute(''' select * from users u join relation r on (u.user_id=r.sender_id) 
                        where receiver_id=%s and state_of_relation = 'follow' ''',
                       [user_id])
        follower_list = namedtuplefetchall(cursor)
        # print(all_users[0].USERNAME)
    args = {
        'user_id': user_id,
        'follower_list': follower_list,
        'following_list': following_list,
    }
    return render(request, template_name, args)


###
def friend_request(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'user_account/friend_request.html'
    my_id = request.session['user_id']
    with connection.cursor() as cursor:
        cursor.execute('''select * from users u join relation r on (u.user_id=r.sender_id) 
                        where receiver_id=%s and state_of_relation = 'follow' ''',
                       [my_id])
        follower_list = namedtuplefetchall(cursor)
        args = {
            'my_id': my_id,
            'follower_list': follower_list,
        }
    return render(request, template_name, args)


def request_accept(request, user_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'user_account/request_accept.html'
    my_id = request.session['user_id']
    with connection.cursor() as cursor:
        cursor.execute(''' update relation set state_of_relation = 'friend' where (sender_id=%s and receiver_id=%s)
                        or (receiver_id=%s and sender_id=%s) ''',
                       [my_id, user_id, my_id, user_id])
        cursor.execute('''select username from users where user_id=%s''', [user_id])
        name = namedtuplefetchall(cursor)[0].USERNAME
        args = {
            'state': 'accepted!',
            'username': name,
            'u_id': user_id,
        }
    return render(request, template_name, args)


def request_reject(request, user_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'user_account/request_accept.html'
    my_id = request.session['user_id']
    with connection.cursor() as cursor:
        cursor.execute(''' delete from relation where ((sender_id=%s and receiver_id=%s)
                        or (receiver_id=%s and sender_id=%s)) ''',
                       [my_id, user_id, my_id, user_id])
        cursor.execute('''select username from users where user_id=%s''', [user_id])
        name = namedtuplefetchall(cursor)[0].USERNAME
        args = {
            'state': 'rejected.',
            'username': name,
            'u_id': user_id,
        }
    return render(request, template_name, args)


def delete_friend(request, user_id):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'user_account/friend_deleted.html'
    my_id = request.session['user_id']
    with connection.cursor() as cursor:
        cursor.execute(''' delete from relation where ((sender_id=%s and receiver_id=%s)
                        or (receiver_id=%s and sender_id=%s)) ''',
                       [my_id, user_id, my_id, user_id])
        cursor.execute('''select username from users where user_id=%s''', [user_id])
        name = namedtuplefetchall(cursor)[0].USERNAME
        args = {
            'state': 'deleted.',
            'username': name,
            'u_id': user_id,
        }
    return render(request, template_name, args)


def relation_home(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'user_account/connectivity.html'
    my_id = request.session['user_id']
    args = {
        'my_id': my_id,
    }
    return render(request, template_name, args)


def deactivate_account(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'user_account/deactivate_account.html'
    with connection.cursor() as cursor:
        cursor.execute(''' update users set account_state='inactive', online_check='offline' where user_id=%s''',
                       [request.session['user_id']])

        args = {}
    try:
        del request.session['user_id']
    except:
        pass

    return render(request, template_name)


def notification(request):
    if not request.session.has_key('user_id'):
        return render(request, 'user_account/goto_login.html')

    template_name = 'user_account/notification.html'

    my_id = request.session['user_id']
    notification_date_limit = 2
    with connection.cursor() as cursor:
        cursor.execute(''' select g.* from group_member m join groups g on(g.group_id=m.group_id)
                        where member_id=%s and ((sysdate-join_date) between 0 and %s )''',
                       [my_id,notification_date_limit])
        groups = namedtuplefetchall(cursor)

        cursor.execute(''' select  e.* from event_table e join event_participants p on(e.event_id=p.event_id)
                        where state_of_participation='invited' and
                        member_id=%s and ((sysdate-join_date) between 0 and %s )''',
                       [my_id, notification_date_limit])
        events = namedtuplefetchall(cursor)

        cursor.execute(''' select * from relation r join users u on(r.sender_id=u.user_id) 
                        where receiver_id=%s 
                        and ((sysdate-first_contact_date_time) between 0 and %s )
                        and state_of_relation='follow' ''',
                       [my_id, notification_date_limit])
        requests = namedtuplefetchall(cursor)

        cursor.execute(''' select e.* from event_table e join event_participants p on(e.event_id=p.event_id) 
                        where ((e.event_date_time-sysdate) between -1 and %s ) and member_id=%s ''',
                       [notification_date_limit,my_id])
        today_events = namedtuplefetchall(cursor)

        #print(today_events)

        args = {
            'groups': groups,
            'events': events,
            'requests': requests,
            'today_events': today_events,
        }

    return render(request, template_name, args)

